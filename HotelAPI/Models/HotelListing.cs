﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace HotelAPI.Models
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class HotelListing
    {
        List<Hotel> hotelList;
        static HotelListing hotellst = null;

        private HotelListing()
        {
            hotelList = new List<Hotel>();
        }

        [WebMethod]
        public static HotelListing getInstance()
        {
            if (hotellst == null)
            {
                hotellst = new HotelListing();
                return hotellst;
            }
            else
            {
                return hotellst;
            }
        }

        [WebMethod]
        public void Add(Hotel hotel)
        {
            hotelList.Add(hotel);
        }

        [WebMethod]
        public String Remove(String hotelID)
        {
            for (int i = 0; i < hotelList.Count; i++)
            {
                Hotel hot = hotelList.ElementAt(i);
                if (hot.ID.Equals(hotelID))
                {
                    hotelList.RemoveAt(i); //update new record
                    return "Delete Successful";
                }
            }
            return "Delete Un-Successful";
        }

        [WebMethod]
        public List<Hotel> GetAllHotels()
        {
            return hotelList;
        }

        [WebMethod]
        public String UpdateHotel(Hotel hot)
        {
            for (int i = 0; i < hotelList.Count; i++)
            {
                Hotel hotel1 = hotelList.ElementAt(i);
                if (hotel1.ID.Equals(hot.ID))
                {
                    hotelList[i] = hot; //updating new record
                    return "Record Updated";
                }
            }
            return "Record Update Un-Successful";
        }
    }
}