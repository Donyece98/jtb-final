﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services; 

namespace HotelAPI.Models
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class HotelReply
    {
        String hotelID;
        String hotelname;
        String location;
        int roomAmt;        
        float roomCost;
        Boolean all_inclusive;
        String regStatus;

        public String ID
        {
            [WebMethod]
            get { return hotelID; }
            [WebMethod]
            set { hotelID = value; }
        }

        public String Name
        {
            [WebMethod]
            get { return hotelname; }
            [WebMethod]
            set { hotelname = value; }
        }

        public String Location
        {
            [WebMethod]
            get { return location; }
            [WebMethod]
            set { location = value; }
        }

        public int RoomAmt
        {
            [WebMethod]
            get { return roomAmt; }
            [WebMethod]
            set { roomAmt = value; }
        }
        
        public float RoomPrice
        {
            [WebMethod]
            get { return roomCost; }
            [WebMethod]
            set { roomCost = value; }
        }
        
        public Boolean AllInclusive
        {
            [WebMethod]
            get { return all_inclusive; }
            [WebMethod]
            set { all_inclusive = value; }
        }
        
        public String RegistrationStatus
        {
            [WebMethod]
            get { return regStatus; }
            [WebMethod]
            set { regStatus = value; }
        }
    }
}