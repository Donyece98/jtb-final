﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelAPI.Models
{
    public class Hotel
    {
        string hotelID;
        string hotelname;
        string location;
        int roomAmt;
        float roomCost;
        Boolean all_inclusive;

        public string ID
        {
            get { return hotelID; }
            set { hotelID = value; }
        }
        public string Name
        {
            get { return hotelname; }
            set { hotelname = value; }
        }

        public string Location
        {
            get { return location; }
            set { location = value; }
        }

        public int RoomAmt
        {
            get { return roomAmt; }
            set { roomAmt = value; }
        }

        
        public float RoomPrice
        {
            get { return roomCost; }
            set { roomCost = value; }
        }
        
        public Boolean AllInclusive
        {
            get { return all_inclusive; }
            set { all_inclusive = value; }
        }
    }
}