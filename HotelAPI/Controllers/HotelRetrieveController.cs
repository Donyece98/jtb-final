﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using HotelAPI.Models; 

namespace HotelAPI.Controllers
{
    public class HotelRetrieveController : ApiController
    {
        [HttpGet]
        public List<Hotel> GetAllHotel()
        {
            return HotelListing.getInstance().GetAllHotels();
        }
    }
}
