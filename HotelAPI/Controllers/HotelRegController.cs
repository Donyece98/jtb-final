﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using HotelAPI.Models; 

namespace HotelAPI.Controllers
{
    public class HotelRegController : ApiController
    {
        [HttpPost]
        public HotelReply addHotel(Hotel hoteladd)
        {
            //Console.WriteLine("In Add Hotel");
            HotelReply hotreply = new HotelReply();
            HotelListing.getInstance().Add(hoteladd);
            hotreply.ID = hoteladd.ID;
            hotreply.Name = hoteladd.Name;
            hotreply.Location = hoteladd.Location;
            hotreply.RoomAmt = hoteladd.RoomAmt;
            hotreply.RoomPrice = hoteladd.RoomPrice;
            hotreply.AllInclusive = hoteladd.AllInclusive;

            return hotreply;
        }
    }
}
