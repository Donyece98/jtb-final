﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="JTB._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div>
   
        <div class="jumbotron text-center">
            <h1>Welcome to Dreamy Destination</h1>
            <p>Booking Agent</p>
        </div>
    </div>

    <%--content body--%>
    <div class="container body-content">
        <%--<asp:ContentPlaceHolder ID="Contentbody" runat="server"></asp:ContentPlaceHolder>--%>
    </div>

    <%--footer--%>
    <%-- </div>--%>
    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <h6>About</h6>
                    <p class="text-justify">
                        Let us help you start your search for cheap airfare and travel deals. 
                            Whether it’s a romantic weekend getaway, family vacation, bucket-list trip to your dream destination,
                            or a business trip with a side of fun, we have you covered. Affordable domestic and international flights
                            are what we do best.
                       
                    </p>
                    <address>
                        Visit us at:<a href="/Index.aspx"> dreamydestinations.com</a>
                        <br>
                        Shop 60-62 Kingston<br>
                        Mall Ocean Blvd.<br>
                        Kingston
                       
                    </address>
                    <address>
                        Email us at <a href="mailto:info@dreamydestinations.com">info@dreamydestinations.com</a><br>
                        Tell: <a href="tel:+13115552368">1-800-555-2368</a>
                    </address>
                </div>

                <div class="col-xs-6 col-md-3">
                    <h6>Services</h6>
                    <ul class="footer-links">
                        <li><a href="/Booking.aspx">Vacation Booking</a></li>
                        <li><a href="/Booking.aspx">Hotel Resevations</a></li>
                        <li><a href="/Booking.aspx">Flight Booking</a></li>
                        <br />
                        <li><a href="/Register.aspx">Login</a></li>
                        <li><a href="/Register.aspx">Signup</a></li>
                    </ul>
                </div>

                <div class="col-xs-6 col-md-3">
                    <h6>Quick Links</h6>
                    <ul class="footer-links">
                        <li><a href="/About.aspx">About</a></li>
                        <li><a href="/Contact.aspx">Contact Us</a></li>
                        <li><a href="/Booking.aspx">Book Now</a></li>
                        <li><a href="#">Contribute</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
            <hr style="color: #000">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-6 col-xs-12">
                    
                </div>
            </div>
        </div>
    </footer>


</asp:Content>
