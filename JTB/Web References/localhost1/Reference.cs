﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
// 
#pragma warning disable 1591

namespace JTB.localhost1 {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3062.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="HotelListingSoap", Namespace="http://tempuri.org/")]
    public partial class HotelListing : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback AddOperationCompleted;
        
        private System.Threading.SendOrPostCallback RemoveOperationCompleted;
        
        private System.Threading.SendOrPostCallback GetAllHotelsOperationCompleted;
        
        private System.Threading.SendOrPostCallback UpdateHotelOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public HotelListing() {
            this.Url = global::JTB.Properties.Settings.Default.JTB_localhost1_HotelListing;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event AddCompletedEventHandler AddCompleted;
        
        /// <remarks/>
        public event RemoveCompletedEventHandler RemoveCompleted;
        
        /// <remarks/>
        public event GetAllHotelsCompletedEventHandler GetAllHotelsCompleted;
        
        /// <remarks/>
        public event UpdateHotelCompletedEventHandler UpdateHotelCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/Add", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public void Add(Hotel hotel) {
            this.Invoke("Add", new object[] {
                        hotel});
        }
        
        /// <remarks/>
        public void AddAsync(Hotel hotel) {
            this.AddAsync(hotel, null);
        }
        
        /// <remarks/>
        public void AddAsync(Hotel hotel, object userState) {
            if ((this.AddOperationCompleted == null)) {
                this.AddOperationCompleted = new System.Threading.SendOrPostCallback(this.OnAddOperationCompleted);
            }
            this.InvokeAsync("Add", new object[] {
                        hotel}, this.AddOperationCompleted, userState);
        }
        
        private void OnAddOperationCompleted(object arg) {
            if ((this.AddCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.AddCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/Remove", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string Remove(string hotelID) {
            object[] results = this.Invoke("Remove", new object[] {
                        hotelID});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void RemoveAsync(string hotelID) {
            this.RemoveAsync(hotelID, null);
        }
        
        /// <remarks/>
        public void RemoveAsync(string hotelID, object userState) {
            if ((this.RemoveOperationCompleted == null)) {
                this.RemoveOperationCompleted = new System.Threading.SendOrPostCallback(this.OnRemoveOperationCompleted);
            }
            this.InvokeAsync("Remove", new object[] {
                        hotelID}, this.RemoveOperationCompleted, userState);
        }
        
        private void OnRemoveOperationCompleted(object arg) {
            if ((this.RemoveCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.RemoveCompleted(this, new RemoveCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/GetAllHotels", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Hotel[] GetAllHotels() {
            object[] results = this.Invoke("GetAllHotels", new object[0]);
            return ((Hotel[])(results[0]));
        }
        
        /// <remarks/>
        public void GetAllHotelsAsync() {
            this.GetAllHotelsAsync(null);
        }
        
        /// <remarks/>
        public void GetAllHotelsAsync(object userState) {
            if ((this.GetAllHotelsOperationCompleted == null)) {
                this.GetAllHotelsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAllHotelsOperationCompleted);
            }
            this.InvokeAsync("GetAllHotels", new object[0], this.GetAllHotelsOperationCompleted, userState);
        }
        
        private void OnGetAllHotelsOperationCompleted(object arg) {
            if ((this.GetAllHotelsCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAllHotelsCompleted(this, new GetAllHotelsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/UpdateHotel", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string UpdateHotel(Hotel hot) {
            object[] results = this.Invoke("UpdateHotel", new object[] {
                        hot});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void UpdateHotelAsync(Hotel hot) {
            this.UpdateHotelAsync(hot, null);
        }
        
        /// <remarks/>
        public void UpdateHotelAsync(Hotel hot, object userState) {
            if ((this.UpdateHotelOperationCompleted == null)) {
                this.UpdateHotelOperationCompleted = new System.Threading.SendOrPostCallback(this.OnUpdateHotelOperationCompleted);
            }
            this.InvokeAsync("UpdateHotel", new object[] {
                        hot}, this.UpdateHotelOperationCompleted, userState);
        }
        
        private void OnUpdateHotelOperationCompleted(object arg) {
            if ((this.UpdateHotelCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.UpdateHotelCompleted(this, new UpdateHotelCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3062.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class Hotel {
        
        private string idField;
        
        private string nameField;
        
        private string locationField;
        
        private int roomAmtField;
        
        private int availRoomsField;
        
        private float roomPriceField;
        
        private string checkInField;
        
        private string checkOutField;
        
        private bool poolField;
        
        private bool allInclusiveField;
        
        private bool childrenField;
        
        private bool spaField;
        
        /// <remarks/>
        public string ID {
            get {
                return this.idField;
            }
            set {
                this.idField = value;
            }
        }
        
        /// <remarks/>
        public string Name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
        
        /// <remarks/>
        public string Location {
            get {
                return this.locationField;
            }
            set {
                this.locationField = value;
            }
        }
        
        /// <remarks/>
        public int RoomAmt {
            get {
                return this.roomAmtField;
            }
            set {
                this.roomAmtField = value;
            }
        }
        
        /// <remarks/>
        public int AvailRooms {
            get {
                return this.availRoomsField;
            }
            set {
                this.availRoomsField = value;
            }
        }
        
        /// <remarks/>
        public float RoomPrice {
            get {
                return this.roomPriceField;
            }
            set {
                this.roomPriceField = value;
            }
        }
        
        /// <remarks/>
        public string CheckIn {
            get {
                return this.checkInField;
            }
            set {
                this.checkInField = value;
            }
        }
        
        /// <remarks/>
        public string CheckOut {
            get {
                return this.checkOutField;
            }
            set {
                this.checkOutField = value;
            }
        }
        
        /// <remarks/>
        public bool Pool {
            get {
                return this.poolField;
            }
            set {
                this.poolField = value;
            }
        }
        
        /// <remarks/>
        public bool AllInclusive {
            get {
                return this.allInclusiveField;
            }
            set {
                this.allInclusiveField = value;
            }
        }
        
        /// <remarks/>
        public bool Children {
            get {
                return this.childrenField;
            }
            set {
                this.childrenField = value;
            }
        }
        
        /// <remarks/>
        public bool Spa {
            get {
                return this.spaField;
            }
            set {
                this.spaField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3062.0")]
    public delegate void AddCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3062.0")]
    public delegate void RemoveCompletedEventHandler(object sender, RemoveCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3062.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class RemoveCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal RemoveCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3062.0")]
    public delegate void GetAllHotelsCompletedEventHandler(object sender, GetAllHotelsCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3062.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetAllHotelsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GetAllHotelsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public Hotel[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((Hotel[])(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3062.0")]
    public delegate void UpdateHotelCompletedEventHandler(object sender, UpdateHotelCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3062.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class UpdateHotelCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal UpdateHotelCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591