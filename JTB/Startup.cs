﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(JTB.Startup))]
namespace JTB
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
