﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Services;
using HotelAPI.Models;
using System.Data;


namespace JTB
{
    public partial class home : System.Web.UI.Page
    {
        private SqlConnection conn = new SqlConnection("Data Source=(LocalDb)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\db044432\\jtb-project\\JTB\\App_Data\\aspnet-JTB-20191109080616.mdf;Initial Catalog=aspnet-JTB-20191109080616;Integrated Security=True");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_ddlCountry();
                Bind_ddlCountry1();
                Bind_ddlState1(); 
            }
        }

        protected void bookBtn_Click(object sender, EventArgs e)
        {
            
        }
        public void Bind_ddlCountry()
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand("SELECT Country, Id FROM Country", conn);
            SqlDataReader dr = cmd.ExecuteReader();
            ddlcountry.DataSource = dr;
            ddlcountry.Items.Clear();
            ddlcountry.Items.Add("--Please select country--");
            ddlcountry.DataTextField = "Country";
            ddlcountry.DataValueField = "Id";
            ddlcountry.DataBind();
            conn.Close();
        }
        public void Bind_ddlCountry1()
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand("SELECT Country, Id FROM Country", conn);
            SqlDataReader dr = cmd.ExecuteReader();
            ddlcountry1.DataSource = dr;
            ddlcountry1.Items.Clear();
            ddlcountry1.Items.Add("--Please select country--");
            ddlcountry1.DataTextField = "Country";
            ddlcountry1.DataValueField = "Id";
            ddlcountry1.DataBind();
            conn.Close();
        }
        
        public void Bind_ddlState()
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand("select state, stateID from CountryState where CountryId= '" + ddlcountry.SelectedValue + "'", conn);
            SqlDataReader dr = cmd.ExecuteReader();
            ddlstate.DataSource = dr;
            ddlstate.Items.Clear();
            ddlstate.Items.Add("--Please select state--");
            ddlstate.DataTextField = "state";
            ddlstate.DataValueField = "stateID";
            ddlstate.DataBind();
            conn.Close();
        }
        
        public void Bind_ddlState1()
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand("select state, stateID from CountryState", conn);
            SqlDataReader dr = cmd.ExecuteReader();
            ddlstate1.DataSource = dr;
            ddlstate1.Items.Clear();
            ddlstate1.Items.Add("--Please select state--");
            ddlstate1.DataTextField = "state";
            ddlstate1.DataValueField = "stateID";
            ddlstate1.DataBind();
            conn.Close();
        }
        
        protected void ddlcountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            Bind_ddlState();
        }

        protected void hotel_btn_Click(object sender, EventArgs e)
        {
            Response.Redirect("Hotel.aspx");
        }

        protected void flightbtn_Click(object sender, EventArgs e)
        {

        }
    }
}